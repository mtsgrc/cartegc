from django.conf.urls import patterns, include, url
from django.conf import settings
from django.contrib import admin
from cartgc.maindb import views
from django.contrib import auth
from django.contrib.staticfiles.urls import staticfiles_urlpatterns
import os

admin.autodiscover()

urlpatterns = patterns('cartgc.maindb',

    # Home
    url(r'^$', 'views.gethome', name='gethome'),

    # Statics files section
    url(r'^favicon\.ico$', 'django.views.generic.simple.redirect_to', {'url': '/media/favicon.ico'}),
    url(r'^static/(?P<path>.*)$', 'django.views.static.serve', {'document_root': settings.STATICFILES_DIRS}),

    # Admin section
    url(r'^admin/', include(admin.site.urls)),

    # Web-Admin contact
    url(r'^getcontact/$', 'views.getcontact'),

    # carteles
    url(r'^signs/$', 'views.cartel_viewall', name='cartel_viewall',kwargs={'view':"list"}),
    url(r'^signs/(?P<view>(list|icons))/$', 'views.cartel_viewall', name='cartel_viewall'),
    url(r'^sign/new/$', 'views.cartel_add', name='cartel_add'),
    url(r'^sign/delete/(?P<idc>\d+)/$', 'views.cartel_delete', name='cartel_delete'),
    url(r'^sign/view/(?P<idc>\d+)/$', 'views.cartel_view', name='cartel_view'),
    url(r'^sign/view/(?P<idc>\d+)/boxsign/$', 'views.boxsign', name="boxsign"),
    url(r'^sign/view/(?P<idc>\d+)/listsign/$', 'views.listsign', name="listsign"),
    url(r'^sign/view/(?P<idc>\d+)/lastact/$', 'views.xCartel_lastact', name="xCartel_lastact"),
    url(r'^sign/view/(?P<idc>\d+)/statusbox/$', 'views.xCartel_statusbox', name="xCartel_statusbox"),
    url(r'^sign/view/(?P<idc>\d+)/lastip/$', 'views.xCartel_lastip', name="xCartel_lastip"),
    url(r'^sign/edit/(?P<idc>\d+)/$', 'views.cartel_edit', name='cartel_edit'),
    url(r'^sign/edit/(?P<idc>\d+)/deletetask/(?P<idt>\d+)/$', 'views.cartel_edit_deltask', name='cartel_edit_deltask'),
    url(r'^sign/edit/(?P<idc>\d+)/deletetasks/$', 'views.cartel_edit_deltasks', name='cartel_edit_deltasks'),
    url(r'^sign/edit/(?P<idc>\d+)/addtask/(?P<taskchrs>[a-z|0-9]+)/$', 'views.cartel_edit_addtask', name='cartel_edit_addtask'),

    # Tasks
    url(r'^tasks/$', 'views.tasks', name='tasks'),
    url(r'^tasks/del/(?P<typedel>(all|inc|prg))/$', 'views.carteles_deltasks', name='carteles_deltasks'),

    # mensajes
    url(r'^messages/$','views.mensaje_viewall', name='mensajes_viewall'),
    url(r'^message/edit/(?P<idm>\d+)/$', 'views.mensaje_edit', name='mensaje_edit'),
    url(r'^message/new/$', 'views.mensaje_new', name='mensaje_new'),
    url(r'^message/delete/(?P<idm>\d+)/$', 'views.mensaje_del', name='mensaje_del'),

    # Paquete de mensajes
    url(r'^messages/packages/$','views.packages_viewall', name='packages_viewall'),
    url(r'^messages/package/new/$','views.package_new', name='package_new'),
    url(r'^messages/package/edit/(?P<idp>\d+)/$','views.package_edit', name='package_edit'),
    url(r'^messages/package/delete/(?P<idp>\d+)/$','views.package_delete', name='package_delete'),

    # Historiales
    url(r'^log/(?P<dyear>\d{4})/(?P<dmonth>\d{2})/(?P<dday>\d{2})(?:/(?P<dsign>\d+))?/$', 'views.logdatesign', name='logdatesign'),

    # Config Web
    url(r'^config/$','views.config_web', name='config_web'),

    # Log In
    url(r'^login/$','views.login', name='login'),
    url(r'^logout/$','views.logout', name='logout'),

    url(r'^static/(?P<path>.*)$', 'django.views.static.serve', {'document_root': settings.STATIC_ROOT, 'show_indexes': settings.DEBUG}),

)

