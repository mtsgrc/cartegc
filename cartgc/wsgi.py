#!/usr/bin/env python
# -*- coding: utf-8 -*-

import os, sys

path = "/var/www/carteleria"

if path not in sys.path:
  sys.path.append(path)

os.environ.setdefault("DJANGO_SETTINGS_MODULE", "cartgc.settings")

from django.core.wsgi import get_wsgi_application
application = get_wsgi_application()

