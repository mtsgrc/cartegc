# -*- encoding: utf-8 -*-
TIME_MINSECS = 60
TIME_HOURSECS = TIME_MINSECS * TIME_MINSECS
TIME_DAYSECS = TIME_HOURSECS * 24
TIME_MONTHSECS = TIME_DAYSECS * 30
TIME_YEARSECS = TIME_MONTHSECS * 12

TYPESYS="G"

AVAILABLE_MESSAGES = False
AVAILABLE_STATS = False
AVAILABLE_PACKAGES = False

REFRESH_TIME = 10

STATUS00 = "INHABILITADO"
STATUS01 = "BROADCASTING"
STATUS02 = "INHABILITADO"
STATUS03 = "INHABILITADO"
STATUS04 = "INHABILITADO"
STATUS05 = "INHABILITADO"
STATUS06 = "POWER"
STATUS07 = "INHABILITADO"

ERRORS00 = "PANELES"
ERRORS01 = "COMUNICACIÓN"
ERRORS02 = "INHABILITADO"
ERRORS03 = "INHABILITADO"
ERRORS04 = "INHABILITADO"
ERRORS05 = "INHABILITADO"
ERRORS06 = "INHABILITADO"
ERRORS07 = "INHABILITADO"

CHARSAVAIBLE='ABCDEFGHIJKLMNÑOPQRSTUVWXYZ0123456789 .-º'
CHARSMAX=100

DEFAULTSCROLL="Rápido"
DEFAULTFONT="Font 01"
DEFAULTTIEMPOMUESTRA="10 seg."

CONFIG00 = "Modo Normal"
CONFIG01 = "Modo Broadcasting"
CONFIG02 = "Modo Sleep"
CONFIG03 = "Mostrar Mensajes"
CONFIG04 = "Mostrar Temperatura"
CONFIG05 = "Mostrar Hora"
CONFIG06 = ""
CONFIG07 = ""


CHRSTASKS = ["b0","b1","m","l","chr","csr","cce","csh", "r"]
CHRSTASKSKEEP = ["b1","m", "l"]
typesTasks = {"b0": "Desactivar Broadcasting",
              "b1": "Activar Broadcasting",
              "m": "Enviar mensajes",
              "l": "Enviar configuracion de brillo",
              "chr": "Reiniciar cartel a modo fabrica",
              "csr": "Reiniciar cartel",
              "cce": "Borrar indicadores de error",
              "csh": "Sincronizar hora",
              "r": "Leer estado de cartel"
              }
