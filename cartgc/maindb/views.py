# -*- encoding: utf-8 -*-
from django.http import HttpResponseRedirect, Http404, HttpResponse
from django.shortcuts import render_to_response
from django.core.mail import send_mail
from django.template.response import TemplateResponse
from django.contrib import auth
from django.contrib.auth.models import User
from django.contrib.auth.decorators import login_required
from django.shortcuts import redirect

from cartgc.maindb.models import MaindbCartel, MaindbBrillo

from cartgc.maindb.models import MaindbMensaje, MaindbTiposcroll, MaindbTiempomuestra, MaindbTipofont, MaindbTask

from cartgc.maindb.models import MaindbPackage

from cartgc.maindb.forms import ContactForm, MensajeForm, CartelForm, PackageForm

from django.forms.models import modelform_factory
from django import forms

from cartgc.maindb.optcfg import *

from datetime import datetime
from django.utils import timezone
import time
import pytz
import traceback
import os
import re
import hashlib
from django import template
from os import walk
import os.path
import collections

register = template.Library()
diractual = os.path.dirname(os.path.dirname(os.path.dirname(os.path.abspath(__file__))))

def appendLog(idcartel,txtdescr,txtdata=""):
    dirlogs = diractual + "/logs"

    diryear = dirlogs + "/" + str(datetime.now().year)
    dirmonth = diryear + "/" + str(datetime.now().month).zfill(2)
    dirday = dirmonth + "/" + str(datetime.now().day).zfill(2)

    if os.path.isdir(dirlogs) == False:
      os.mkdir(dirlogs)

    if os.path.isdir(diryear) == False:
      os.mkdir(diryear)

    if os.path.isdir(dirmonth) == False:
      os.mkdir(dirmonth)

    if os.path.isdir(dirday) == False:
      os.mkdir(dirday)

    pathfile = dirday + "/" + str(idcartel)
    logtime = u"%s:%s:%s.%s" % (str(datetime.now().hour).zfill(2),
                               str(datetime.now().minute).zfill(2),
                               str(datetime.now().second).zfill(2),
                               str(datetime.now().microsecond / 1000).zfill(3))

    with open(pathfile, "a") as logfile:
      logfile.write((u"[%s] %s %s\n" % (logtime, txtdescr,[""," (%s)" % (txtdata)][txtdata != ""])).encode('utf-8'))

def showDatetime():
    datetimeNow = datetime.datetime.now()
    return render_to_response('datetime.html', locals())

def utc2local (utc):
  epoch = time.mktime(utc.timetuple())
  offset = datetime.fromtimestamp (epoch) - datetime.utcfromtimestamp (epoch)
  return utc + offset

def gethome(request):

    if not request.user.is_authenticated():
      return HttpResponseRedirect('/login/?next=%s' % request.path)

    Messages_available = AVAILABLE_MESSAGES
    actUser = request.user
    return HttpResponseRedirect('/signs/')

def getcontact(request):

    if not request.user.is_authenticated():
      return HttpResponseRedirect('/login/?next=%s' % request.path)

    Messages_available = AVAILABLE_MESSAGES
    dataUser = request.user

    if request.method == 'POST':
      form = ContactForm(request.POST)

      if form.is_valid():
        post_data = request.POST.copy()
        asunto = post_data.get('asunto','')
        mensaje = post_data.get('mensaje','')
        remitente = post_data.get('remitente', 'anon@anon.com')

        if len(mensaje.split()) >= 4:
          send_mail('CarteleriaWeb. Asunto: %s' % asunto, mensaje, remitente, ['maati.garcia@gmail.com'])
          form = ContactForm()
          mjeOK = "Su correo ha sido enviado"
        else:
          form = ContactForm(request.POST)
          mjeError = "El mensaje es muy corto"

    else:
      form = ContactForm()

    return TemplateResponse(request,'getcontact.html', locals())

def cartel_viewall(request,view):

    if not request.user.is_authenticated():
      return HttpResponseRedirect('/login/?next=%s' % request.path)

    Messages_available = AVAILABLE_MESSAGES
    allCarteles = MaindbCartel.objects.all()
    allTasks = MaindbTask.objects.all()
    countSigns = len(allCarteles)
    cantSignsRow = 4
    signsWithTasks = []
    Stats_available = AVAILABLE_STATS
    for xTask in allTasks:
      if xTask.status == "i":
        if not xTask.sign.id in signsWithTasks:
          signsWithTasks.append(xTask.sign.id)

    if view == 'list':
        return render_to_response('carteles.html', locals())
    elif view == 'icons':
        return render_to_response('carteles2.html', locals())
    else:
        raise Http404

def cartel_view(request,idc):

    if not request.user.is_authenticated():
      return HttpResponseRedirect('/login/?next=%s' % request.path)

    Stats_available = AVAILABLE_STATS
    Messages_available = AVAILABLE_MESSAGES

    try:

        xCartel = MaindbCartel.objects.get(id=idc)
        showBrightness = False

        if TYPESYS == "C":
          showBrightness = True

        allBrillos = MaindbBrillo.objects.all()
        allMensajes = MaindbMensaje.objects.all()

        if xCartel.laststatus != None and xCartel.laststatus > 0:
          broadcastingStat = [False,True][(xCartel.laststatus & 2**1) == 2**1]
        else:
          broadcastingStat = False

        allTasks = MaindbTask.objects.filter(sign=xCartel, status='i')
        descripTasks = typesTasks


    except MaindbCartel.DoesNotExist:
        raise Http404

    return TemplateResponse(request,'cartel/view.html', locals())

def tasks(request):

    if not request.user.is_authenticated():
      return HttpResponseRedirect('/login/?next=%s' % request.path)

    Messages_available = AVAILABLE_MESSAGES
    Packages_available = AVAILABLE_PACKAGES

    try:

        allCarteles = MaindbCartel.objects.all()
        allBrillos = MaindbBrillo.objects.all()
        allMensajes = MaindbMensaje.objects.all()
        allPackages = MaindbPackage.objects.all()

        strAllPackages = {}
        choicesTypesTasks = {}

        # Por cada paquete
        for xPckg in allPackages:
          arrPckgMssgs = xPckg.messages.split(',')
          strMssgsPckg = {}
          cMssgs = 0
          for xMssgPckg in arrPckgMssgs:
            for xMssg in allMensajes:
              if xMssgPckg == str(xMssg.idmessage):
                strMssgsPckg[cMssgs] = xMssg.contentmessage
            cMssgs = cMssgs + 1
          strAllPackages[xPckg.id] = strMssgsPckg

        # Agregamos todas las tareas (con excepciones)
        for valTypeTask, descTypeTask in typesTasks.items():
          if valTypeTask == "l":
            if TYPESYS == "C":
              choicesTypesTasks[valTypeTask] = descTypeTask
          else:
            choicesTypesTasks[valTypeTask] = descTypeTask

        # Largo maximo para broadcasting
        maxLengthText = CHARSMAX

        if request.method == 'POST':

          post_data = request.POST.copy()

          post_task = post_data.get("chrstask","")
          post_signs = post_data.get("signstxt","")
          post_date = post_data.get("txtprogdate","")
          post_mssg = post_data.get("sourcemessages","")
          post_broadcasting = post_data.get("txtbroadcasting","")
          dtprog = None
          if post_date:
            dtprog = pytz.timezone('America/Argentina/Mendoza').localize(datetime.strptime(post_date, '%Y-%m-%dT%H:%M')).astimezone(pytz.utc)

          empty_signs = False
          empty_broadcasting = False
          empty_mssg = False

          if not post_signs:
            empty_signs = True

          if not post_broadcasting and post_task == "b1":
            empty_broadcasting = True

          if not post_mssg and post_task == "m":
            empty_mssg = True

          if empty_mssg == True or empty_broadcasting == True or empty_signs == True:
            return TemplateResponse(request,'cartel/addtask.html', locals())

          # Si tarea es valida
          if post_task in CHRSTASKS:

            # Array de carteles con tarea a agregar
            signs_arr = post_signs.split(',')

            for dataSign in signs_arr:
              yCartel = MaindbCartel.objects.get(id=int(dataSign))

              if yCartel:

                dateNow = datetime.now()
                if (len(MaindbTask.objects.all())==0):
                  newTask_id = 1
                else:
                  newTask_id = MaindbTask.objects.latest('id').id + 1
                newTask = MaindbTask(id=newTask_id, sign=yCartel, datejoin=dateNow, todate=dtprog, task=post_task, status='i')

                if post_task == "m":

                  txtmessages = ""

                  if post_mssg != "":
                    txtmessages = None
                    if post_mssg[0:1] == "c":
                      for sCartel in allCarteles:
                        if sCartel.id == int(post_mssg[1:]):
                          txtmessages = sCartel.lastmessages
                          break
                    elif post_mssg[0:1] == "p":
                      for sPackage in allPackages:
                        if sPackage.id == int(post_mssg[1:]):
                          txtmessages = sPackage.messages
                          break
                    else:
                      pass

                    if txtmessages:
                      if dtprog == None:
                        yCartel.lastmessages = txtmessages
                        yCartel.save()
                        appendLog(yCartel.id, "Modificacion nueva: Mensajes", txtmessages)
                      else:
                        newTask.content = txtmessages


                elif post_task == "b1":
                  if dtprog == None:
                    yCartel.lastbroadcasting = post_broadcasting
                    appendLog(yCartel.id, "Modificacion nueva: Broadcasting", post_broadcasting)
                    yCartel.save()
                  else:
                    newTask.content = post_broadcasting

                else:
                  pass

                newTask.save()

                strdata = ""
                if post_date:
                  post_date_formated = datetime.strftime(pytz.timezone('America/Argentina/Mendoza').localize(datetime.strptime(post_date, '%Y-%m-%dT%H:%M')),'%d/%m/%Y %H:%M')
                  strdata += "Fecha: " + post_date_formated
                if newTask.content:
                  strdata += [""," - "][len(strdata)>0] + "Contenido: " + newTask.content

                newTask_date = pytz.timezone('America/Argentina/Mendoza').localize(dateNow).astimezone(pytz.utc)
                appendLog(yCartel.id, "Tarea nueva %s(t%s): %s" % (["","programada "][not not post_date],str(newTask.id).zfill(6), typesTasks[post_task]), strdata)

          return HttpResponseRedirect('/signs/')

    except:
      tb = traceback.format_exc()
      return HttpResponse(tb)
      #raise Http404

    return TemplateResponse(request,'cartel/addtask.html', locals())

def carteles_deltasks(request, typedel):
    if not request.user.is_authenticated():
      return HttpResponseRedirect('/login/?next=%s' % request.path)

    try:
      if typedel == "inc":
        allTasks = MaindbTask.objects.filter(status='i').exclude(todate__isnull=False)
      elif typedel == "prg":
        allTasks = MaindbTask.objects.filter(status='i').exclude(todate__isnull=True)
      elif typedel == "all":
        allTasks = MaindbTask.objects.filter(status='i')
      else:
        allTasks = []

      if allTasks:
        for xTask in allTasks:
          appendLog(xTask.sign.id, "Tarea eliminada (t%s): %s" % (str(xTask.id).zfill(6), typesTasks[xTask.task]))
          xTask.delete()

      return HttpResponseRedirect('/signs/')

    except:
      raise Http404


def cartel_edit_deltask(request,idc,idt):

    if not request.user.is_authenticated():
      return HttpResponseRedirect('/login/?next=%s' % request.path)

    Messages_available = AVAILABLE_MESSAGES
    try:

        allTasks = MaindbTask.objects.all()
        xCartel = MaindbCartel.objects.get(id=idc)
        for xTask in allTasks:
          if xTask.sign == xCartel and xTask.status== 'i' and xTask.id == int(idt):
            xTask_id = xTask.id
            xTask.delete()
            dateNow = xTask.datejoin
            appendLog(xCartel.id, "Tarea eliminada (t%s): %s" % (str(xTask_id).zfill(6), typesTasks[xTask.task]))

    except MaindbCartel.DoesNotExist:
        raise Http404

    return HttpResponseRedirect(('/sign/view/%s/' % xCartel.id))

def cartel_edit_deltasks(request,idc):

    if not request.user.is_authenticated():
      return HttpResponseRedirect('/login/?next=%s' % request.path)

    Messages_available = AVAILABLE_MESSAGES
    try:
        allTasks = MaindbTask.objects.all()
        xCartel = MaindbCartel.objects.get(id=idc)
        xCount = 0
        tasksDel = []
        for xTask in allTasks:
          if xTask.sign == xCartel and xTask.status== 'i':
            tmpID = xTask.id
            tmpDSC = typesTasks[xTask.task]
            xTask.delete()
            xCount += 1
            tasksDel.append("t%s: %s" % (str(tmpID).zfill(6), tmpDSC))
        appendLog(xCartel.id,["Tarea eliminada","Tareas eliminadas"][xCount != 1] + "(" + ', '.join([str(x) for x in tasksDel]) + ")")

    except MaindbCartel.DoesNotExist:
        raise Http404

    return HttpResponseRedirect(('/sign/view/%s/' % xCartel.id))

def cartel_delete(request,idc):

    if not request.user.is_authenticated():
      return HttpResponseRedirect('/login/?next=%s' % request.path)

    Messages_available = AVAILABLE_MESSAGES
    try:

        # Objeto del Id que se pasa como argumento
        xCartel = MaindbCartel.objects.get(id=idc)

        # Borramos mensaje
        xCartel.delete()
        appendLog(xCartel.id, "Cartel eliminado")

        # Reenviamos
        return HttpResponseRedirect('/signs/')

    except:

        # Mandamos a 404
        raise Http404

def cartel_edit(request,idc):

    if not request.user.is_authenticated():
      return HttpResponseRedirect('/login/?next=%s' % request.path)

    Messages_available = AVAILABLE_MESSAGES

    try:

        xCartel = MaindbCartel.objects.get(id=idc)
        allBrillos = MaindbBrillo.objects.all()
        allMensajes = MaindbMensaje.objects.all()
        allPackages = MaindbPackage.objects.all()
        strAllPackages = {}
        for xPckg in allPackages:
          arrPckgMssgs = xPckg.messages.split(',')
          strMssgsPckg = {}
          cMssgs = 0
          for xMssgPckg in arrPckgMssgs:
            for xMssg in allMensajes:
              if xMssgPckg == str(xMssg.idmessage):
                strMssgsPckg[cMssgs] = xMssg.contentmessage
            cMssgs = cMssgs + 1
          strAllPackages[xPckg.id] = strMssgsPckg

        showBrightness = False
        charsAvaible = CHARSAVAIBLE

        lastname = xCartel.namecartel
        lastmessages = xCartel.lastmessages
        lastbroadcasting = xCartel.lastbroadcasting

        if TYPESYS == "C":
          showBrightness = True

        CartelFormSet = modelform_factory(MaindbCartel, form=CartelForm,
                                widgets={
                                    'id': forms.NumberInput(attrs={'readonly': 'readonly',}),
                                    'namecartel': forms.TextInput(attrs={'size': 70}),
                                    'lastbrightness': forms.Select(choices=allBrillos),
                                    'lastmessages': forms.Textarea(attrs={'cols': 50, 'rows':5,'readonly': 'readonly',}),
                                },
                        )

        if request.method == 'POST':
            post_data = request.POST.copy()
            form = CartelForm(post_data, request.FILES, instance=xCartel)

            # Si esta todo OK
            if form.is_valid():

              # Guardamos
              form.save()

              strdata = ""

              if lastname != xCartel.namecartel:
                strdata += ["",", "][len(strdata)!=0] + ("Nombre cartel: %s > %s" % (lastname,xCartel.namecartel))
              if lastmessages != xCartel.lastmessages:
                strdata += ["",", "][len(strdata)!=0] + ("Mensajes cartel: %s > %s" % (lastmessages, xCartel.lastmessages))

              appendLog(xCartel.id, "Cartel modificado",strdata)

              # Redirigimos a la vision del cartel
              return HttpResponseRedirect(('/sign/view/%s/' % xCartel.id))

            else:

              form_errors = form.errors
              form = CartelFormSet(instance=xCartel)

        else:

            form = CartelFormSet(instance=xCartel)

    except MaindbCartel.DoesNotExist:
        raise Http404

    return TemplateResponse(request,'cartel/edit.html', locals())

def cartel_edit_addtask(request,idc,taskchrs):

    if not request.user.is_authenticated():
      return HttpResponseRedirect('/login/?next=%s' % request.path)

    Messages_available = AVAILABLE_MESSAGES
    try:
        xCartel = MaindbCartel.objects.get(id=int(idc))
        if taskchrs == "m":
          timePreTask = datetime.now()
          dtprog = pytz.timezone('America/Argentina/Mendoza').localize(timePreTask).astimezone(pytz.utc)
          if (len(MaindbTask.objects.all())==0):
            preTask_id = 1
          else:
            preTask_id = MaindbTask.objects.latest('id').id + 1
          preTask = MaindbTask(id=preTask_id, sign=xCartel, datejoin=timePreTask, task="b0", status='i')
          preTask.save()
          appendLog(xCartel.id, "Tarea nueva (t%s): %s" % (str(preTask_id).zfill(6), typesTasks[preTask.task]))

        for xTypeTask_chrs, xTypeTask_dsc in typesTasks.items():
          if taskchrs == xTypeTask_chrs:
            task_descpr = xTypeTask_dsc
            timeNewTask = datetime.now()
            dtprog = pytz.timezone('America/Argentina/Mendoza').localize(timeNewTask).astimezone(pytz.utc)
            if (len(MaindbTask.objects.all())==0):
              newTask_id = 1
            else:
              newTask_id = MaindbTask.objects.latest('id').id + 1
            newTask = MaindbTask(id=newTask_id, sign=xCartel, datejoin=timeNewTask, task=taskchrs, status='i')
            newTask.save()
            appendLog(xCartel.id, "Tarea nueva (t%s): %s" % (str(newTask_id).zfill(6), typesTasks[newTask.task]))

    except MaindbCartel.DoesNotExist:
        raise Http404

    return HttpResponseRedirect(('/sign/view/%s/' % xCartel.id))


def cartel_add(request):

    if not request.user.is_authenticated():
      return HttpResponseRedirect('/login/?next=%s' % request.path)

    Messages_available = AVAILABLE_MESSAGES
    try:

        # Valores de Campos
        allBrillos = MaindbBrillo.objects.all()
        allCarteles = MaindbCartel.objects.all()
        noIDSAvaible = []

        # Valores de ID que no puede tomar el nuevo cartel
        for xCartel in allCarteles:
            noIDSAvaible.append(xCartel.id)

        # Formulario del Mensaje
        CartelFormSet = modelform_factory(MaindbCartel, form=CartelForm,
                                    widgets={
                                        'id': forms.NumberInput(attrs={'readonly': 'readonly',}),
                                        'lastbrightness': forms.Select(choices=allBrillos),
                                        'namecartel': forms.TextInput(attrs={'size': 50})
                                    },
                         )

        if request.method == 'POST':
            post_data = request.POST
            form = CartelFormSet(post_data)

            if form.is_valid():

                form.save()
                return HttpResponseRedirect("/signs/")

            else:

                form = CartelFormSet()

        else:

            form = CartelFormSet()

        return TemplateResponse(request,'cartel/add.html', locals())

    except:
        # Mandamos a 404
        raise Http404

def xCartel_lastip(request,idc):

    try:
        xCartel = MaindbCartel.objects.get(id=idc)
        return render_to_response('cartel/lastip.html',locals())
    except MaindbMensaje.DoesNotExist:
        raise Http404

def xCartel_statusbox(request,idc):

    try:
        xCartel = MaindbCartel.objects.get(id=idc)
        return render_to_response('cartel/statusbox.html',locals())
    except MaindbMensaje.DoesNotExist:
        raise Http404

def xCartel_lastact(request,idc):

    try:
        xCartel = MaindbCartel.objects.get(id=idc)
        return render_to_response('cartel/lastact.html',locals())
    except MaindbMensaje.DoesNotExist:
        raise Http404

def listsign(request,idc):

    Messages_available = AVAILABLE_MESSAGES
    if not request.user.is_authenticated():
      return HttpResponseRedirect('/login/?next=%s' % request.path)

    try:
        xCartel = MaindbCartel.objects.get(id=idc)
        return render_to_response('cartel/listsign.html',locals())
    except MaindbMensaje.DoesNotExist:
        raise Http404

def boxsign(request,idc):

    try:
        xCartel = MaindbCartel.objects.get(id=idc)
        return render_to_response('cartel/boxsign.html',locals())
    except MaindbMensaje.DoesNotExist:
        raise Http404

def mensaje_viewall(request):

    if not request.user.is_authenticated():
      return HttpResponseRedirect('/login/?next=%s' % request.path)


    Messages_available = AVAILABLE_MESSAGES
    Packages_available = AVAILABLE_PACKAGES
    allMensajes = MaindbMensaje.objects.all()

    return render_to_response('mensajes.html', locals())

def mensaje_del(request,idm):

    if not request.user.is_authenticated():
      return HttpResponseRedirect('/login/?next=%s' % request.path)

    Messages_available = AVAILABLE_MESSAGES
    try:

        # Instanciamos todos los carteles
        allCarteles = MaindbCartel.objects.all()

        # Objeto del Id que se pasa como argumento
        xMensaje = MaindbMensaje.objects.get(idmessage=idm)

        # Borramos mensaje de los carteles donde se encuentra
        for xCartel in allCarteles:

            # Si contiene mensajes
            if xCartel.lastmessages:

                # Borramos
                xCartel.delMssgFromCartel(str(idm))

        # Borramos mensaje
        xMensaje.delete()

        # Reenviamos
        return HttpResponseRedirect('/messages/')

    except:

        # Mandamos a 404
        raise Http404

def mensaje_new(request):

    if not request.user.is_authenticated():
      return HttpResponseRedirect('/login/?next=%s' % request.path)

    Messages_available = AVAILABLE_MESSAGES
    # Valores de Campos
    allTiposcroll = MaindbTiposcroll.objects.all()
    allTiempomuestra = MaindbTiempomuestra.objects.all()
    allTipofont = MaindbTipofont.objects.all()

    defaultTiposcroll = DEFAULTSCROLL
    defaultTiempomuestra = DEFAULTTIEMPOMUESTRA
    defaultTipofont = DEFAULTFONT

    showTiposcroll = False
    showTiempomuestra = False
    showTipofont = False
    charsAvaible = CHARSAVAIBLE

    if TYPESYS == "G":
      showTiposcroll = True
      showTipofont = True

    if TYPESYS == "C":
      showTiempomuestra = True

    # Formulario del Mensaje
    MensajeFormSet = modelform_factory(MaindbMensaje, form=MensajeForm,
                        widgets={
                            'idmessage': forms.NumberInput(attrs={'readonly': 'readonly',}),
                            'contentmessage': forms.TextInput(attrs={'size': 70}),
                            'typescrolling': forms.Select(choices=allTiposcroll),
                            'timemessage': forms.Select(choices=allTiempomuestra),
                            'typefont': forms.Select(choices=allTipofont),
                        },
             )

    if request.method == 'POST':
        post_data = request.POST
        form = MensajeFormSet(post_data)

        if form.is_valid():

            form.save()
            return HttpResponseRedirect("/messages/")

        else:

            form_errors=form.errors
            form = MensajeFormSet()

    else:

        form = MensajeFormSet()

    return TemplateResponse(request,'mensaje/add.html', locals())


def mensaje_edit(request,idm):

    if not request.user.is_authenticated():
      return HttpResponseRedirect('/login/?next=%s' % request.path)

    Messages_available = AVAILABLE_MESSAGES
    showTiposcroll = False
    showTiempomuestra = False
    showTipofont = False
    charsAvaible = CHARSAVAIBLE
    dataTime = datetime.now()
    dataTemp = "27"

    if TYPESYS == "G":
      showTiposcroll = True
      showTipofont = True

    if TYPESYS == "C":
      showTiempomuestra = True

    try:

        # Objeto del Id que se pasa como argumento
        xMensaje = MaindbMensaje.objects.get(idmessage=idm)

        # Valores de Campos
        allTiposcroll = MaindbTiposcroll.objects.all()
        allTiempomuestra = MaindbTiempomuestra.objects.all()
        allTipofont = MaindbTipofont.objects.all()

        # Formulario del Mensaje
        MensajeFormSet = modelform_factory(MaindbMensaje, form=MensajeForm,
                                widgets={
                                    'idmessage': forms.NumberInput(attrs={'readonly': 'readonly',}),
                                    'contentmessage': forms.TextInput(attrs={'size': 70,}),
                                    'typescrolling': forms.Select(choices=allTiposcroll),
                                    'timemessage': forms.Select(choices=allTiempomuestra),
                                    'typefont': forms.Select(choices=allTipofont),
                                },
                     )


        if request.method == 'POST':
            post_data = request.POST.copy()
            form = MensajeForm(post_data, request.FILES, instance=xMensaje)

            if form.is_valid():

                form.save()
                return HttpResponseRedirect('/messages/')

            else:
                form_errors= form.errors
                form = MensajeFormSet(instance=xMensaje)

        else:

            form = MensajeFormSet(instance=xMensaje)

    except MaindbMensaje.DoesNotExist:
        raise Http404

    return TemplateResponse(request,'mensaje/edit.html', locals())

def packages_viewall(request):

    if not request.user.is_authenticated():
      return HttpResponseRedirect('/login/?next=%s' % request.path)

    Messages_available = AVAILABLE_MESSAGES
    allPackages = MaindbPackage.objects.all()
    return render_to_response('paquetes.html', locals())

def package_edit(request,idp):

    if not request.user.is_authenticated():
      return HttpResponseRedirect('/login/?next=%s' % request.path)

    Messages_available = AVAILABLE_MESSAGES
    try:
        xPackage = MaindbPackage.objects.get(id=idp)
        allMensajes = MaindbMensaje.objects.all()

        PackageFormSet = modelform_factory(MaindbPackage, form=PackageForm,
                                widgets={
                                        'id': forms.NumberInput(attrs={'readonly': 'readonly',}),
                                        'namepackage': forms.TextInput(attrs={'size': 70}),
                                        'messages': forms.Textarea(attrs={'cols': 50, 'rows':5,'readonly': 'readonly',}),
                                },
                        )

        if request.method == 'POST':
            post_data = request.POST.copy()
            form = PackageForm(post_data, request.FILES, instance=xPackage)

            if form.is_valid():

                form.save()
                return HttpResponseRedirect('/messages/packages/')

            else:

                form_errors = form.errors
                form = PackageFormSet(instance=xPackage)

        else:

            form = PackageFormSet(instance=xPackage)

    except MaindbPackage.DoesNotExist:
        raise Http404

    return TemplateResponse(request,'paquete/edit.html', locals())

def package_delete(request,idp):

    if not request.user.is_authenticated():
      return HttpResponseRedirect('/login/?next=%s' % request.path)

    Messages_available = AVAILABLE_MESSAGES
    try:

        # Objeto del Id que se pasa como argumento
        xPackage = MaindbPackage.objects.get(id=idp)

        # Borramos mensaje
        xPackage.delete()

        # Reenviamos
        return HttpResponseRedirect('/messages/packages/')

    except:

        # Mandamos a 404
        raise Http404

def package_new(request):

  if not request.user.is_authenticated():
    return HttpResponseRedirect('/login/?next=%s' % request.path)

  Messages_available = AVAILABLE_MESSAGES
  try:
    allMensajes = MaindbMensaje.objects.all()

    PackageFormSet = modelform_factory(MaindbPackage, form=PackageForm,
                        widgets={
                          'id': forms.NumberInput(attrs={'readonly': 'readonly',}),
                          'namepackage': forms.TextInput(attrs={'size': 70}),
                          'messages': forms.Textarea(attrs={'cols': 50, 'rows':5,'readonly': 'readonly',}),
                          },
                      )

    if request.method == 'POST':

        post_data = request.POST
        form = PackageFormSet(post_data)

        if form.is_valid():

          form.save()
          return HttpResponseRedirect("/messages/packages/")

        else:

          form = PackageFormSet()

    else:

      form = PackageFormSet()

    return TemplateResponse(request,'paquete/add.html', locals())

  except:
    raise Http404

def logdatesign(request,dyear,dmonth,dday, dsign=None):

  if not request.user.is_authenticated():
    return HttpResponseRedirect('/login/?next=%s' % request.path)

  try:
    dataSign = None
    allCarteles = MaindbCartel.objects.all()
    dateGetStr = str(dyear) + "/" + str(dmonth) + "/" + str(dday)
    filesInDir = []
    pathOnSearch = diractual + "/logs/" + dateGetStr
    dateToday = datetime.strftime(datetime.now(), "%Y/%m/%d")
    for (dirpath, dirnames, filenames) in walk(pathOnSearch):
      filesInDir.extend(filenames)
      break
    filesToList = list(map(int, filesInDir))
    filesToList.sort()
    if dsign:
      try:
        contentFile = {}
        nameCartel = MaindbCartel.objects.get(id=int(dsign)).namecartel
        dateModified = None
        with open(pathOnSearch + "/" + dsign) as objFile:
          linesFile = objFile.readlines()
          for xLineFile in linesFile:
            tmpLine = xLineFile.split("] ",1)
            contentFile[tmpLine[0][1:]] = tmpLine[1]
        contentFile = collections.OrderedDict(sorted(contentFile.items()))
      except:
        linesFile = []
        contentFile = {}
    return TemplateResponse(request,'log/view.html', locals())
  except Exception as exc:
    # Mandamos a 404
    raise Http404
    #return HttpResponse(str(exc))

def logout(request):
    try:
      auth.logout(request)
    except KeyError:
      pass
    return HttpResponseRedirect('/login/')

def login(request):

    if request.method == 'POST':
      if request.session.test_cookie_worked():      # Testear si hay cookies habilitadas
        request.session.delete_test_cookie()                # Borrar cookie de prueba
        post_data = request.POST.copy()
        post_user = post_data.get("cusername","")
        post_upwd = post_data.get("cpassword","")
        post_keep = post_data.get("ckeeplogin","")
        post_nurl = post_data.get("cnexturl","")
        objUser = auth.authenticate(username=post_user, password=post_upwd)

        if objUser is not None and objUser.is_active:
          auth.login(request, objUser)
          if post_nurl:
            return HttpResponseRedirect(post_nurl)
          else:
            return HttpResponseRedirect("/")
        else:
          mjeError = "Los datos ingresados son incorrectos"
      else:
        mjeError = "No hay cookies habilitadas, por favor, habilite las cookies y reintente de nuevo"
    else:
      nexturl = request.GET.get("next","")
      request.session["dataUser"] = "datauser"
      del request.session["dataUser"]

    if request.user.is_authenticated():
      thisSession = "yes"
    else:
      thisSession = "no"
    request.session.set_test_cookie()                             # Cookie de prueba

    return TemplateResponse(request,'login.html', locals())

def config_web(request):
    if not request.user.is_authenticated():
      return HttpResponseRedirect('/login/?next=%s' % request.path)
    try:
      dataUser = request.user
      if request.method == "POST":
        post_data = request.POST.copy()
        data_newpwd = post_data.get("usr_newpwd","")
        data_newemail = post_data.get("usr_newemail", "")
        data_actpwd = post_data.get("usr_actpwd", "")
        if dataUser.check_password(data_actpwd):
          if data_newemail:
            if re.match('^[(a-z0-9\_\-\.)]+@[(a-z0-9\_\-\.)]+\.[(a-z)]{2,4}$',data_newemail.lower()):
              dataUser.email = data_newemail
            else:
              mjeError_reemail = "Ha ingresado un correo invalido"
          if data_newpwd:
            dataUser.set_password(data_newpwd)
          dataUser.save()
        else:
          mjeError_wremail = "Ha ingresado una clave incorrecta"
    except:
      mjeError = "Ha ocurrido un error"
    return TemplateResponse(request,'config.html', locals())
