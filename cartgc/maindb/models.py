# This is an auto-generated Django model module.
# You'll have to do the following manually to clean this up:
#   * Rearrange models' order
#   * Make sure each model has one field with primary_key=True
#   * Remove `managed = False` lines for those models you wish to give write DB access
# Feel free to rename the models, but don't rename db_table values or field names.
#
# Also note: You'll have to insert the output of 'django-admin.py sqlcustom [appname]'
# into your database.
from __future__ import unicode_literals

from django.db import models
import datetime
import time

from cartgc.maindb.optcfg import *



def local_to_utc(t):
    secs = time.mktime(t)
    return time.gmtime(secs)

def utc_to_local(t):
    secs = calendar.timegm(t)
    return time.localtime(secs)


class AuthGroup(models.Model):
    id = models.IntegerField(primary_key=True)
    name = models.CharField(unique=True, max_length=80)
    class Meta:
        managed = False
        db_table = 'auth_group'

class AuthGroupPermissions(models.Model):
    id = models.IntegerField(primary_key=True)
    group_id = models.IntegerField()
    permission = models.ForeignKey('AuthPermission')
    class Meta:
        managed = False
        db_table = 'auth_group_permissions'

class AuthPermission(models.Model):
    id = models.IntegerField(primary_key=True)
    name = models.CharField(max_length=50)
    content_type_id = models.IntegerField()
    codename = models.CharField(max_length=100)
    class Meta:
        managed = False
        db_table = 'auth_permission'

class AuthUser(models.Model):
    id = models.IntegerField(primary_key=True)
    password = models.CharField(max_length=128)
    last_login = models.DateTimeField()
    is_superuser = models.BooleanField()
    username = models.CharField(unique=True, max_length=30)
    first_name = models.CharField(max_length=30)
    last_name = models.CharField(max_length=30)
    email = models.CharField(max_length=75)
    is_staff = models.BooleanField()
    is_active = models.BooleanField()
    date_joined = models.DateTimeField()
    class Meta:
        managed = False
        db_table = 'auth_user'

class AuthUserGroups(models.Model):
    id = models.IntegerField(primary_key=True)
    user_id = models.IntegerField()
    group = models.ForeignKey(AuthGroup)
    class Meta:
        managed = False
        db_table = 'auth_user_groups'

class AuthUserUserPermissions(models.Model):
    id = models.IntegerField(primary_key=True)
    user_id = models.IntegerField()
    permission = models.ForeignKey(AuthPermission)
    class Meta:
        managed = False
        db_table = 'auth_user_user_permissions'

class DjangoAdminLog(models.Model):
    id = models.IntegerField(primary_key=True)
    action_time = models.DateTimeField()
    user_id = models.IntegerField()
    content_type_id = models.IntegerField(blank=True, null=True)
    object_id = models.TextField(blank=True)
    object_repr = models.CharField(max_length=200)
    action_flag = models.PositiveSmallIntegerField()
    change_message = models.TextField()
    class Meta:
        managed = False
        db_table = 'django_admin_log'

class DjangoContentType(models.Model):
    id = models.IntegerField(primary_key=True)
    name = models.CharField(max_length=100)
    app_label = models.CharField(max_length=100)
    model = models.CharField(max_length=100)
    class Meta:
        managed = False
        db_table = 'django_content_type'

class DjangoSession(models.Model):
    session_key = models.CharField(unique=True, max_length=40)
    session_data = models.TextField()
    expire_date = models.DateTimeField()

    class Meta:
        managed = False
        db_table = 'django_session'

class MaindbBrillo(models.Model):

    id = models.IntegerField(primary_key=True)
    percentbrightness = models.PositiveSmallIntegerField(db_column='percentBrightness') # Field name made lowercase.
    valuebrightness = models.PositiveSmallIntegerField(db_column='valueBrightness') # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'maindb_brillo'

    def __unicode__(self):
        return "%s %c" % (self.percentbrightness,chr(37))


class MaindbCartel(models.Model):

    id = models.IntegerField(primary_key=True)
    namecartel = models.CharField(db_column='nameCartel', max_length=50, blank=True) # Field name made lowercase.
    laststatus = models.IntegerField(db_column='lastStatus', blank=True, null=True) # Field name made lowercase.
    lasterrors = models.IntegerField(db_column='lastErrors', blank=True, null=True) # Field name made lowercase.
    lasttime = models.DateTimeField(db_column='lastTime', blank=True, null=True) # Field name made lowercase.
    lasttemp = models.IntegerField(db_column='lastTemp', blank=True, null=True) # Field name made lowercase.
    lastbrightness = models.ForeignKey(MaindbBrillo, db_column='lastBrightness_id', blank=True, null=True) # Field name made lowercase.
    lastmessages = models.CharField(db_column='lastMessages', max_length=300, blank=True) # Field name made lowercase.
    lastbroadcasting = models.CharField(db_column='lastBroadcasting', max_length=CHARSMAX, blank=True, null=True) # Field name made lowercase.
    lastip = models.CharField(db_column='lastIP', max_length=15, blank=True, null=True) # Field name made lowercase.
    lastlogin = models.DateTimeField(db_column='lastLogin', blank=True, null=True) # Field name made lowercase.
    lastreading = models.DateTimeField(db_column='lastReading', blank=True, null=True) # Field name made lowercase.
    taskstodo = models.TextField(db_column='tasksToDo', blank=True, null=True) # Field name made lowercase.
    historylog = models.TextField(db_column='historyLog', blank=True, null=True) # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'maindb_cartel'

    def cleanCartel(self):
        self.laststatus = None
        self.lasterrors = None
        self.lasttime = None
        self.lasttemp = None
        self.lastbrightness = None
        self.lastmessages = ""
        self.lastbroadcasting = None
        self.lastip = None
        self.lastlogin = None
        self.lastreading = None
        self.tasktodo = None
        self.historylog = None
        self.save()

    def getDefStatus(self):
        defStatus = {'bit7': STATUS07,
                     'bit6': STATUS06,
                     'bit5': STATUS05,
                     'bit4': STATUS04,
                     'bit3': STATUS03,
                     'bit2': STATUS02,
                     'bit1': STATUS01,
                     'bit0': STATUS00,
                    }
        return defStatus

    def getDefErrors(self):
        defErrors = {'bit7': ERRORS07,
                     'bit6': ERRORS06,
                     'bit5': ERRORS05,
                     'bit4': ERRORS04,
                     'bit3': ERRORS03,
                     'bit2': ERRORS02,
                     'bit1': ERRORS01,
                     'bit0': ERRORS00,
                    }
        return defErrors

    def getStatusCartel(self):
        statebits = {'bit7': False, 'bit6': False, 'bit5': False, 'bit4': False, 'bit3': False, 'bit2': False, 'bit1': False, 'bit0': False}
        if self.laststatus is not None:
          for i in range(0,8):
            statebits["bit"+str(i)] = self.laststatus & (2**i) != 0
        return statebits

    def getErrorsCartel(self):
        errorsbits = {'bit7': False, 'bit6': False, 'bit5': False, 'bit4': False, 'bit3': False, 'bit2': False, 'bit1': False, 'bit0': False}
        if self.lasterrors is not None:
            for i in range(0,8):
                errorsbits["bit"+str(i)] = self.lasterrors & (2**i) != 0
        return errorsbits

    def getElapsedTimeFrom(self,src_datetime,typeret="arr",blacklist="",shortxt=True):

        diff_datetime = {strhours: 0, strminutes: 0, strseconds: 0}

        if src_datetime:
          time_srcdt = time.mktime(src_datetime.timetuple())
          time_datetimenow = time.mktime(time.gmtime())
          diff_datetimes = time_datetimenow - time_srcdt

          diff_hours = int(diff_datetimes / TIME_HOURSECS)
          diff_minutes = int(diff_datetimes / TIME_MINSECS) % TIME_MINSECS
          diff_seconds = int(diff_datetimes) % TIME_MINSECS

          if typeret=="arr":

            diff_datetime = {strhours: diff_hours, strminutes: diff_minutes, strseconds: diff_seconds}
            return diff_datetime

          else:

            if shortxt==True:
              strhours = "h"
              strminutes = "m"
              strseconds = "s"
            else:
              strhours = "horas"
              strminutes = "minutos"
              strseconds = "segundos"

            str_part_h = ["","%d%s" % (diff_hours, strhours)][("h" in blacklist) == False]
            str_part_m = ["","%d%s" % (diff_minutes, strminutes)][("m" in blacklist) == False]
            str_part_s = ["","%d%s" % (diff_seconds, strseconds)][("s" in blacklist) == False]

            str_dtret = ""

            if str_part_h:
              str_dtret = str_part_h
            if str_part_m:
              str_dtret = [str_dtret + " " + str_part_m,str_part_m][str_dtret == ""]
            if str_part_s:
              str_dtret = [str_dtret + " " + str_part_s,str_part_s][str_dtret == ""]

            return str_dtret


    def getElapsedTimeFromLastLogin(self):
        diff_datetime = {'h': 0, 'm': 0, 's': 0}
        if self.lastlogin is not None:
            time_lastlogin = time.mktime(self.lastlogin.timetuple())
            time_datetimenow = time.mktime(time.gmtime())
            diff_datetimes = time_datetimenow - time_lastlogin

            diff_hours = int(diff_datetimes / TIME_HOURSECS)
            diff_minutes = int(diff_datetimes / TIME_MINSECS) % TIME_MINSECS
            diff_seconds = int(diff_datetimes) % TIME_MINSECS

            diff_datetime = {'h': diff_hours, 'm': diff_minutes, 's': diff_seconds}
        return diff_datetime

    def getElapsedTimeFromLastReading(self):
        diff_datetime = {'h': 0, 'd': 0, 'm': 0, 's': 0}
        if self.lastreading is not None:
            time_lastreading = time.mktime(self.lastreading.timetuple())
            time_datetimenow = time.mktime(time.gmtime())
            diff_datetimes = time_datetimenow - time_lastreading

            diff_hours = int(diff_datetimes / TIME_HOURSECS)
            diff_minutes = int(diff_datetimes / TIME_MINSECS) % TIME_MINSECS
            diff_seconds = int(diff_datetimes) % TIME_MINSECS
            diff_datetime = {'h': diff_hours, 'm': diff_minutes, 's': diff_seconds}

        return diff_datetime

    def delMssgFromCartel(self,idmssg):
        arrMssgs=self.lastmessages.split(',')
        try:
            arrMssgs = (value for value in arrMssgs if value != idmssg and value != '')
            strNewMssgs = ','.join(arrMssgs)
            self.lastmessages = strNewMssgs
            self.save()
            return strNewMssgs
        except ValueError:
            return ','.join(arrMssgs)

    def getMssgOBJ(self):
        try:
            myMSSG = []
            allMensajes = MaindbMensaje.objects.all()
            arrayMSSGcsv = self.lastmessages.split(",")
            for xMSSG in arrayMSSGcsv:
                idToGet = int(xMSSG)
                for xMensaje in allMensajes:
                    if xMensaje.idmessage == idToGet:
                        myMSSG.append(xMensaje)
                        break
            return myMSSG
        except:
            return None

    def getIncompleteTasks(self):
      allTasks = MaindbTask.objects.all()
      for xTask in allTasks:
        if xTask.status == "i" and xTask.sign == self and xTask.todate == None:
          return True
      return False


class MaindbTiempomuestra(models.Model):
    id = models.IntegerField(primary_key=True)
    idname = models.CharField(db_column='idName', max_length=10) # Field name made lowercase.
    valuetime = models.PositiveSmallIntegerField(db_column='valueTime') # Field name made lowercase.
    class Meta:
        managed = False
        db_table = 'maindb_tiempomuestra'

    def __unicode__(self):
        return "%s" % (self.idname)

class MaindbTipofont(models.Model):
    idfont = models.IntegerField(db_column='idFont', primary_key=True) # Field name made lowercase.
    idname = models.CharField(db_column='idName', max_length=20) # Field name made lowercase.
    class Meta:
        managed = False
        db_table = 'maindb_tipofont'

    def __unicode__(self):
        return "%s" % (self.idname)

class MaindbTiposcroll(models.Model):
    id = models.IntegerField(primary_key=True)
    idname = models.CharField(db_column='idName', max_length=20) # Field name made lowercase.
    valuescroll = models.PositiveSmallIntegerField(db_column='valueScroll') # Field name made lowercase.
    class Meta:
        managed = False
        db_table = 'maindb_tiposcroll'

    def __unicode__(self):
        return "%s" % (self.idname)

class MaindbPackage(models.Model):
    id = models.IntegerField(db_column='id', primary_key=True)
    namepackage = models.CharField(db_column='namePackage',max_length=50, blank=True)
    messages = models.CharField(db_column='messages', max_length=300, blank=True) # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'maindb_package'

    def getMssgOBJ(self):
        try:
            myMSSG = []
            allMensajes = MaindbMensaje.objects.all()
            arrayMSSGcsv = self.messages.split(",")
            for xMSSG in arrayMSSGcsv:
                idToGet = int(xMSSG)
                for xMensaje in allMensajes:
                    if xMensaje.idmessage == idToGet:
                        myMSSG.append(xMensaje)
                        break
            return myMSSG
        except:
            return None

class MaindbMensaje(models.Model):
    idmessage = models.IntegerField(db_column='idMessage', primary_key=True) # Field name made lowercase.
    contentmessage = models.CharField(db_column='contentMessage', max_length=CHARSMAX, blank=False) # Field name made lowercase.
    typescrolling = models.ForeignKey('MaindbTiposcroll', db_column='typeScrolling_id', blank=(True,False)[TYPESYS=="G"], null=True) # Field name made lowercase.
    timemessage = models.ForeignKey('MaindbTiempomuestra', db_column='timeMessage_id', blank=(True,False)[TYPESYS=="C"], null=True) # Field name made lowercase.
    typefont = models.ForeignKey('MaindbTipofont', db_column='typeFont_id', blank=(True,False)[TYPESYS=="G"], null=True) # Field name made lowercase.
    descripcion = models.CharField(max_length=200, blank=True)
    class Meta:
        managed = False
        db_table = 'maindb_mensaje'

    def getCarteles(self):
        allCarteles=MaindbCartel.objects.all()
        insideIN = {}
        for xCartel in allCarteles:
            if xCartel.lastmessages is not None:
                arrayMensajes = xCartel.lastmessages.split(',')
                if str(self.idmessage) in arrayMensajes:
                    intCartel = xCartel.id
                    strCartel = "%s - %s" % (xCartel.id, xCartel.namecartel)
                    insideIN[intCartel] = strCartel
        return insideIN

    def getSTRConfig(self):
        textConfig = ""
        if self.typescrolling != None:
            if textConfig != "":
                textConfig = "%s | %s" % (textConfig, self.typescrolling)
            else:
                textConfig = "%s" % (self.typescrolling)

        if self.timemessage != None:
            if textConfig != "":
                textConfig = "%s | %s" % (textConfig, self.timemessage)
            else:
                textConfig = "%s" % (self.timemessage)

        if self.typefont != None:
            if textConfig != "":
                textConfig = "%s | %s" % (textConfig, self.typefont)
            else:
                textConfig = "%s" % (self.typefont)

        return textConfig

class MaindbTask(models.Model):

    id = models.IntegerField(primary_key=True)
    sign = models.ForeignKey(MaindbCartel, db_column='sign_id', blank=False, null=False)
    datejoin = models.DateTimeField(db_column='datejoin', blank=True, null=True)
    todate = models.TextField(db_column='todate', blank=True, null=True)
    task = models.TextField(db_column='task', blank=False, null=False)
    status = models.CharField(db_column='status', max_length=1, blank=False, null=False)
    content = models.TextField(db_column='content', blank=True, null=True)

    class Meta:
      managed = False
      db_table = 'maindb_task'

    def setStatusComplete(self):
      self.status = "c"
      self.save()

    def save(self, *args, **kwargs):
      super(MaindbTask, self).save(*args, **kwargs)
      return self.pk

    def getContent(self):

      if self.content:
        if self.task == "b1":
          return self.content

        elif self.task == "m":
          strret = ""
          arr_sign = self.content.split(',')
          xCounter = 0
          for strSMS in arr_sign:
            xCounter +=1
            for xMssg in MaindbMensaje.objects.all():
              if xMssg.idmessage == int(strSMS):
                if strret:
                  strret += " - " + str(xCounter) + ") " + xMssg.contentmessage
                else:
                  strret = str(xCounter) + ") " + xMssg.contentmessage
                break
            pass
          return strret
        else:
          return ""
      else:
        return ""
