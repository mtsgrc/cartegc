import os

BASE_DIR = os.path.dirname(os.path.dirname(__file__))

if __name__ == "__main__":
    os.environ.setdefault("DJANGO_SETTINGS_MODULE", "cartgc.settings")

# Quick-start development settings - unsuitable for production
# See https://docs.djangoproject.com/en/1.6/howto/deployment/checklist/

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = '+5mn!c6445q6#@*eypn(82a05@9ujj7q$pr76n1g#+#&$r#y9o'

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = True
DEBUG404 = True

ALLOWED_HOSTS=['localhost','127.0.0.1',]

TEMPLATE_DEBUG = DEBUG

# Application definition

INSTALLED_APPS = (
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'django.contrib.staticfiles.urls',
    'cartgc.maindb',
    # socket
    'django_socketio',
)

MIDDLEWARE_CLASSES = (
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
)

ROOT_URLCONF = 'cartgc.urls'

WSGI_APPLICATION = 'cartgc.wsgi.application'


# Database
# https://docs.djangoproject.com/en/1.6/ref/settings/#databases

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3',
        'NAME': os.path.join(BASE_DIR, 'db.sqlite3'),
    }
}

# Internationalization
# https://docs.djangoproject.com/en/1.6/topics/i18n/

# Templates
TEMPLATE_DIRS = (
        os.path.join(BASE_DIR, 'templates'),
)
TEMPLATE_CONTEXT_PROCESSORS = ("django.contrib.auth.context_processors.auth",
)

LANGUAGE_CODE = 'es-ar'

TIME_ZONE = 'America/Argentina/Mendoza'

USE_I18N = True

USE_L10N = True

USE_TZ = True

# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/1.6/howto/static-files/

STATICFILES_DIRS = (os.path.join(os.path.dirname(__file__), 'static'),)

STATICFILES_FINDERS = (
    'django.contrib.staticfiles.finders.FileSystemFinder',
    'django.contrib.staticfiles.finders.AppDirectoriesFinder',
)

STATICFILES_DIRS = (
    os.path.join(os.path.dirname(__file__), 'static'),
)

STATIC_URL = '/static/'
MEDIA_URL = '/static/'

if DEBUG:
  STATIC_ROOT = os.path.join(BASE_DIR, '/static')
else:
  STATIC_ROOT = os.path.join(BASE_DIR, 'static')

#MEDIA_ROOT = '/var/www/carteleria/cartgc/logs/'
#MEDIA_URL = '/var/www/carteleria/cartgc/logs/'

LOGIN_URL = '/login/'

# FILE UPDLOAD
#FILE_UPLOAD_PERMISSIONS = 775


# SESSIONS
SESSION_SAVE_EVERY_REQUEST = True
