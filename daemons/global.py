
#!/usr/bin/env python
# -*- coding: utf-8 -*-

from datetime import datetime

def printDEBUG(mssg):

  if CFG_DEBUG == True:
    mssghc = ""
    for c in mssg:
      if c in CONST_PRINTABLECHARS:
        mssghc += c
      else:
        mssghc += "{" + "{0:#0{1}x}".format(ord(c),4) + "}"

    print "DEBUG: %s" % (mssghc)
    #appendLOG("DEBUG: %s" % (mssghc))

  else:
    pass

def printOUT(mssg, stayinline=False):

    logDATETIME = datetime.now()

    mssghc = ""
    for c in mssg:
      if c in CONST_PRINTABLECHARS:
        mssghc += c
      else:
        mssghc += "{" + "{0:#0{1}x}".format(ord(c),4) + "}"


    if stayinline == True:
      textToPrint = '%02d%02d%02d.%s %s' % (logDATETIME.hour, \
                                                logDATETIME.minute, \
                                                logDATETIME.second, \
                                                str(logDATETIME.microsecond)[:3], \
                                                mssghc),
    else:
      textToPrint = '%02d%02d%02d.%s %s' % (logDATETIME.hour, \
                                                logDATETIME.minute, \
                                                logDATETIME.second, \
                                                str(logDATETIME.microsecond)[:3], \
                                                mssghc)
    print textToPrint
    #appendLOG(textToPrint)

