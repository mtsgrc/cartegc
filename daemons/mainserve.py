#!/usr/bin/env python
# -*- coding: utf-8 -*-

import SocketServer
import socket
import os
import sys, traceback
import re

from dbhandler import *
from datetime import datetime, date
from globalfunc import *

import fcntl
import struct
import select

STRCL_INITTRAMA = '>'
STRCL_ENDTRAMA = '<'
STRCL_EQREQUEST = "EQ"
STRCL_EQREQUESTR = 'R'
STRCL_EQREQUESTOK = '!'
STRCL_EQREQUESTERR = '?'

STRSV_RXOK = "RXOK"
STRSV_FIN = "fin"
STRSV_INITTRAMA = STRCL_INITTRAMA
STRSV_ENDTRAMA = STRCL_ENDTRAMA

SV_MAXRETRIES = 3

cantSessions = 0

CONST_TIMEOUT_CONN = 10.0
CFG_NUMPORT = 5000
RTA_ERRR = "E"


def get_ip_address():
  namehost=socket.getfqdn()
  if namehost=="raspberrypi":
    ifname="eth0"
  else:
    ifname="wlan0"
  s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
  return socket.inet_ntoa(fcntl.ioctl(s.fileno(),0x8915,struct.pack('256s', ifname[:15]))[20:24])

class ThreadedTCPServer(SocketServer.ThreadingMixIn, SocketServer.TCPServer):
  pass

class MyTCPHandler(SocketServer.BaseRequestHandler):

  trySession  = 0
  taskStep = 0
  arrNextTask = []
  objTASK = None

  def setup(self):
    self.request.settimeout(CONST_TIMEOUT_CONN)

  def handle_timeout(self):
    print "Has been passed %s seconds of timeout. Closing connection!" % (str(self.timeout))

  def handle(self):

    try:
      global cantSessions

      cantSessions += 1
      printOUT(" ==== [ SESION " + str(cantSessions) + " ] ==== ")
      printOUT(" [%s] Conexion IP: %s" % (str(cantSessions),self.client_address[0]))
      self.data = ""
      strBuffer = ""
      idSignIdent = False
      objSIGN = None

      while True:

        self.data = self.request.recv(16).strip()                                               # get data

        if not self.data:

          printOUT(" [%s] No se recibieron datos " % (str(cantSessions)))
          strTOsend = STRSV_INITTRAMA + STRSV_RXOK + STRSV_ENDTRAMA
          self.request.sendall(strTOsend)
          printDEBUG("SENT: %s" % (strTOsend))
          break

        else:

          self.usrData = dataSession(self.data)                                                # set data request
          printDEBUG("INPUT: [%s]" % ( self.data.decode("utf-8", "replace") ))

          if self.usrData.isValidData() == True:

            printOUT(" [%s] CARTEL: %s - REQUEST: %s" % ( str(cantSessions), self.usrData.idSign, self.usrData.strPreData ))

            objSIGN = dbSignClass(int(self.usrData.idSign))

            idSignIdent = True

            if not self.arrNextTask:
              self.objTASK = dbTask(int(self.usrData.idSign))
              self.arrNextTask = self.objTASK.getArrTask(cantSessions)                                                         # GET NEXT TASK FROM self.usrData.strPreData
              printDEBUG("READING TASKS (FIRST TIME): [" + ','.join(self.arrNextTask) + "]")
              if not self.arrNextTask:
                printOUT(" [%s] TAREA: Ninguna" % str(cantSessions) )
            else:
              printDEBUG("NEXT STEP: [%s]" % (str(self.taskStep)))

            # >EQXXX!< >EQXXX?<
            if self.usrData.strPreData == STRCL_EQREQUESTOK or self.usrData.strPreData == STRCL_EQREQUESTERR:
              strTOsend = STRSV_INITTRAMA + STRSV_RXOK + STRSV_ENDTRAMA
              self.request.sendall(strTOsend)
              printDEBUG("SENT: [%s]" % ( strTOsend ))

              if self.usrData.strPreData == STRCL_EQREQUESTOK:                                        # Si llego ! pasamos al paso siguiente (si existe)
                self.taskStep += 1                                                                      # Incrementamos paso
                self.trySession = 0                                                                     # Reiniciamos intentos

                if self.usrData.strReadedData:                                                          # Llegaron datos para guardar estado de cartel
                  printDEBUG("DATA: %s" % ( self.usrData.strReadedData.decode("utf-8", "replace") ))
                  resultSetStatus = objSIGN.setStatus(self.usrData.strReadedData)                                         # Guarda estado de cartel
                  if resultSetStatus == None:
                    self.taskStep -= 1

              if self.usrData.strPreData == STRCL_EQREQUESTERR:                                       # Si llego ? repetimos paso, e incrementamos intentos
                self.trySession += 1                                                                    # Incrementamos intento (manteniendo paso)

            # >EQXXXR<
            if self.usrData.strPreData == STRCL_EQREQUESTR:
              if self.arrNextTask:
                if self.arrNextTask[0][0:2] == "#m" and self.trySession == SV_MAXRETRIES:
                  strTOsend = STRSV_INITTRAMA + STRSV_FIN + STRSV_ENDTRAMA
                  self.request.sendall(strTOsend)
                  printDEBUG("SENT: [%s]" % ( strTOsend ))
                  break

                else:
                  strTOsend = STRSV_INITTRAMA + self.arrNextTask[self.taskStep] + STRSV_ENDTRAMA

                  # Se cambia de formato, porque esta todo en unicode, y al enviar un unichr desarma en varios bytes
                  self.request.sendall( strTOsend.encode('cp1252', 'replace') )

                  printDEBUG(b"SENT: [%s]" % ( bytes(strTOsend.encode('ascii', 'replace')) ))
                  printOUT(" [%s] Tarea enviada" % (str(cantSessions)))

                  if self.arrNextTask[self.taskStep] == "fin":                                                # Only for messages
                    self.taskStep += 1                                                                          # Al enviar fin termina trama, no espera EQREQUESTR
                    if self.objTASK.completeTask():
                      printOUT(" [%s] Tarea completa" % (str(cantSessions)))

              else:
                strTOsend = STRSV_INITTRAMA + STRSV_RXOK + STRSV_ENDTRAMA
                self.request.sendall(strTOsend)                                         # ENVIAMOS >RXOK<
                printDEBUG("SENT: [%s]" % ( strTOsend ))


            if self.taskStep < len(self.arrNextTask) and self.trySession < SV_MAXRETRIES:                               # Si no completa las tareas o no llega a los intentos maximos
              printOUT(" [%s] Esperando respuesta..." % (str(cantSessions)))
              continue                                                                                                    # GOTO while on TOP (CONTINUE)
            else:
              if self.trySession == SV_MAXRETRIES and self.arrNextTask[0][0:2] == "#m":
                printOUT(" [%s] Esperando respuesta..." % (str(cantSessions)))
                continue
              else:
                if self.usrData.strPreData == STRCL_EQREQUESTOK:
                  if self.objTASK.completeTask():
                    printOUT(" [%s] Tarea completa" % (str(cantSessions)))
                break

          else:
            printOUT(" [%s] Datos invalidos: %s" % ( str(cantSessions), self.data ))

      if idSignIdent == True:
        if objSIGN:
          objSIGN.setLogin(self.client_address[0])

    except socket.timeout:
      printOUT(" [%s] Tiempo agotado (%s seg.)" % (str(cantSessions),str(CONST_TIMEOUT_CONN)))

    except Exception as e:
      printOUT(" [%s] ERROR: %s" % (str(cantSessions), str(e)))
      traceback.print_exc(file=sys.stdout)
      exit()

    finally:
      printOUT(" [%s] Cerrando conexion" % (str(cantSessions)))
      self.request.close()

    printOUT(" ")

class dataSession(object):

  strData = None
  idSign = None
  strPreData = None
  strReadedData = None
  strResponse = None

  def __init__(self,strcmd):
    self.strData = strcmd
    self.detectData()                                     # Detect data at __init__

  def isValidData(self):

    try:

      if self.strData[0] == STRCL_INITTRAMA and self.strData[-1] == STRCL_ENDTRAMA:
        self.strData = self.strData[1:-1]                                             # Only get {data} (inside): >{data}<
        if re.match('EQ[0-9]{3}[\!|\?|R]', self.strData):                             # If match pattern data
          self.detectData()
          return True
        else:
          return False
      else:
        return False

    except:
      pass

  def detectData(self):
    try:
      self.idSign = self.strData[2:5]                                       # Get idSign
      self.strPreData = self.strData[5]                                     # Get byte preData
      if self.strPreData == STRCL_EQREQUESTOK and len(self.strData) > 6:   # If data exist after !, set it in strReadedData
        self.strReadedData = self.strData[6:]
    except:
      pass


if __name__ == "__main__":

  try:
    printOUT(" ============================= [ INICIO DEMONIO TCP CARTELERIA ] ============================= ")

    if len(sys.argv)>1:
      ipUser = "%s" % (sys.argv[1])
    else:
      ipUser = get_ip_address()

    printOUT("Starting web service atheca on %s:%s " % ( ipUser, CFG_NUMPORT ) )

    HOST, PORT = str(ipUser), CFG_NUMPORT
    SocketServer.TCPServer.allow_reuse_address = True
    SocketServer.TCPServer.timeout = CONST_TIMEOUT_CONN
    server = SocketServer.TCPServer((HOST, PORT), MyTCPHandler)
    server.serve_forever()

  except KeyboardInterrupt:
    server.shutdown()
    exit()

  except Exception, e:
    print str(e)
    exit()

  printOUT("Cerrando aplicacion...\n")

