#!/usr/bin/env python
# -*- coding: utf-8 -*-

import sqlite3
import os
import pytz
import sys, traceback
from datetime import datetime
from globalfunc import *

BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))

pathDB = BASE_DIR + "/db.sqlite3"

CONST_BROADCASTINGCFG = chr(0b11100111)   # Muy rapido por defecto
CONST_ENDTEXT = chr(13)

CONST_TASK_INIT = "#"
CONST_TASK_B0 = "b0"
CONST_TASK_B1 = "b1"
CONST_TASK_M = "m"
CONST_TASK_R = "r"
CONST_TASK_MFIN = "fin"
CONST_TASK_MCFG_BYTE = 0b11100100
CONST_TASK_C = "c"
CONST_TASK_CHR = "chr"
CONST_TASK_CHR_BYTE = chr(230)
CONST_TASK_CCE = "cce"
CONST_TASK_CCE_BYTE = chr(211)
CONST_TASK_CSR = "csr"
CONST_TASK_CSR_BYTE = chr(154)
CONST_TASK_CSH = "csh"
CONST_TASK_CSH_BYTE = chr(182)

CONST_SPLITSMS = ','

cantSessions = 0

class dbSignClass(object):

  idSign = None

  def __init__(self, idSign):
    self.idSign = idSign
    pass

  def setLogin(self, ipClient):
    try:
      conn = sqlite3.connect(pathDB)
      cursor = conn.cursor()
      strQuery = "UPDATE maindb_cartel SET lastLogin='" + datetime.strftime(datetime.utcnow(), "%Y-%m-%d %H:%M:%S") + "', lastIP='" + ipClient  + "' WHERE id=" + str(self.idSign)
      printDEBUG("Query: %s" % (strQuery))

      cursor.execute(strQuery)
      conn.commit()

      if conn.total_changes == 1:
        printDEBUG("Cartel logueado")
        return True
      else:
        printDEBUG("Cartel no logueado")
        return False

    except:
      return None

  def setStatus(self, strData):
    try:

      conn = sqlite3.connect(pathDB)
      cursor = conn.cursor()

      if len(strData) == 8 and strData[-1:] == chr(0xfe):
        dictData = {}
        lastTime_localtime_str = datetime.strftime(datetime.now(), "%Y-%m-%d") + " " + str(ord(strData[0:1])).zfill(2) + ":" + str(ord(strData[1:2])).zfill(2) + ":" + str(ord(strData[2:3])).zfill(2)
        lastTime_localtime_dt = pytz.timezone('America/Argentina/Mendoza').localize(datetime.strptime(lastTime_localtime_str, '%Y-%m-%d %H:%M:%S'))
        dictData['time'] = datetime.strftime(lastTime_localtime_dt.astimezone(pytz.UTC), '%Y-%m-%d %H:%M:%S')
        dictData['errors'] = str(ord(strData[3:4]))
        dictData['status'] = str(ord(strData[4:5]))
        dictData['sms'] = str(ord(strData[5:6]))       # No considerado
        dictData['temp'] = str([ord(strData[6:7]), -(ord(strData[6:7]) & 0x7f)][ord(strData[6:7])>=0x80])

        strQuery = "UPDATE maindb_cartel SET lastStatus='" + dictData['status'] + "'" + \
                                          ", lastErrors='" + dictData['errors'] + "'" + \
                                          ", lastTime='" + dictData['time'] + "'" + \
                                          ", lastTemp='" + dictData['temp'] + "'" + \
                                          ", lastReading='" + datetime.strftime(datetime.utcnow(), "%Y-%m-%d %H:%M:%S") + "'" + \
                                          " WHERE id=" + str(self.idSign)
        printDEBUG("Query: %s" % (strQuery))

        cursor.execute(strQuery)
        conn.commit()

        if conn.total_changes == 1:
          printDEBUG("Cartel actualizado")
          return True
        else:
          printDEBUG("Cartel no actualizado")
          return False

      else:
        printDEBUG("Datos de estado del cartel incorrectos")
        return None

    except Exception, e:
      traceback.print_exc(file=sys.stdout)
      return None


  def getStat(self, arrColumns=None, strCondition=None):
    try:
      arrData = []

      conn = sqlite3.connect(pathDB)
      cursor = conn.cursor()

      if not arrColumns:
        strQuery = "SELECT * FROM maindb_cartel WHERE id=%s%s;" % (str(self.idSign), ["", " AND %s" % (strCondition) ][ strCondition != None ])
      else:
        strColumns = ', '.join(arrColumns)
        strQuery = "SELECT %s FROM maindb_cartel WHERE id=%s%s;" % (strColumns, str(self.idSign), ["", " AND %s" % (strCondition) ][ strCondition != None ])

      printDEBUG("Query: %s" % (strQuery))
      cursor.execute(strQuery)

      for xData in cursor:
        arrData.append(xData)

      return arrData

    except Exception, e:
      traceback.print_exc(file=sys.stdout)
      return None

class dbClass(object):

  def __init__(self):
    pass

  def getData(self, idTable, strCondition, arrColumns=None):
    try:
      arrData = []

      conn = sqlite3.connect(pathDB)
      cursor = conn.cursor()

      if not arrColumns:
        strQuery = "SELECT * FROM %s WHERE %s;" % (idTable, strCondition)
      else:
        strColumns = ', '.join(arrColumns)
        strQuery = "SELECT %s FROM %s WHERE %s;" % (strColumns, idTable, strCondition)

      printDEBUG("Query: %s" % (strQuery))

      cursor.execute(strQuery)
      dataIS = cursor.fetchall()

      for xData in dataIS:
        arrData.append(xData)

      return arrData

    except Exception, e:
      traceback.print_exc(file=sys.stdout)
      return None

class dbTask(object):

  idSign = None
  nextTask = None
  idNextTask = None

  def __init__(self, idSign):
    self.idSign = idSign
    self.objSIGN = dbSignClass(idSign)        # Informacion del cartel

    objDB = dbClass()                         # Para recuperar la proxima tarea

    self.nextTask = objDB.getData("maindb_task", "sign_id=%s and todate is null and status='i' order by todate limit 1" % (str(self.idSign)))

    if self.nextTask:
      self.nextTask = self.nextTask[0]
      self.idNextTask = self.nextTask[0]
    else:
      self.nextTask = None

  def completeTask(self):
    try:
      conn = sqlite3.connect(pathDB)
      cursor = conn.cursor()
      strQuery = "UPDATE maindb_task SET status='c' WHERE id=%s" % (str(self.idNextTask))
      printDEBUG("Query: %s" % (strQuery))
      cursor.execute(strQuery)
      conn.commit()

      if conn.total_changes == 1:
        printDEBUG("Tarea completada")
        appendLog(int(self.idSign), "t" + str(self.idNextTask).zfill(6) + " < Completa!")
      else:
        printDEBUG("Tarea completada no guardada")
        appendLog(int(self.idSign), "t" + str(self.idNextTask).zfill(6) + " < Completa NO GUARDADA!")

    except Exception, e:
      traceback.print_exc(file=sys.stdout)
      return None

  def getArrTask(self,cantSessions):

    try:

      printDEBUG("Proxima tarea: %s" % (str(self.nextTask)))

      if self.nextTask:

        strTask = str(self.nextTask[4])
        arrTask = []

        if strTask:
          if strTask == CONST_TASK_B0:                                                                                        # DESACTIVE DE BROADCASTING
            printOUT(" [%s] TAREA: Desactivacion de Broadcasting" % str(cantSessions) )
            arrTask.append(CONST_TASK_INIT + CONST_TASK_B0)

          elif strTask == CONST_TASK_R:                                                                                      # LECTURA DE ESTADO
            printOUT(" [%s] TAREA: Lectura de estado" % str(cantSessions) )
            arrTask.append(CONST_TASK_INIT + CONST_TASK_R)

          elif strTask == CONST_TASK_B1:                                                                                      # ACTIVE DE BROADCASTING
            dataBroadcasting = self.objSIGN.getStat(["lastBroadcasting"])[0][0]
            if not dataBroadcasting:
              arrTask = []
            else:
              printOUT(" [%s] TAREA: Activacion de Broadcasting, texto '%s'" % (str(cantSessions), dataBroadcasting))
              arrTask.append(CONST_TASK_INIT + CONST_TASK_B1 + CONST_BROADCASTINGCFG.decode('cp1252') + dataBroadcasting + CONST_ENDTEXT)

          elif strTask == CONST_TASK_M:                                                                                       # ENVIO DE MENSAJES
            printOUT(" [%s] TAREA: Actualizacion de Mensajes" % str(cantSessions) )
            strSMS = self.objSIGN.getStat(["lastMessages"])[0][0]
            printDEBUG("Mensajes: %s" % (strSMS))
            arrSMS = strSMS.split(CONST_SPLITSMS)
            tmpSMS = []
            arrTask = []
            arrTask.append(CONST_TASK_INIT + CONST_TASK_M + chr(len(arrSMS)))
            for xSMS in arrSMS:
              printDEBUG(".")
              tmpDB = dbClass()
              tmpSMS = tmpDB.getData("maindb_mensaje", "idMessage=%s" % (str(xSMS)))
              if tmpSMS:
                tmpCFGsms_id = int(tmpSMS[0][2])
                tmpDBScroll = dbClass()
                tmpCFGvalue = tmpDBScroll.getData("maindb_tiposcroll", "id=%s" % (str(tmpCFGsms_id)))
                if tmpCFGvalue:
                  tmpCFGvalue_int = tmpCFGvalue[0][2]
                  if tmpCFGvalue_int == 0:
                    tmpCFGvalue_int = 1
                  chrCFGvalue = chr(ord(chr(CONST_TASK_MCFG_BYTE | (tmpCFGvalue_int - 1))))
                  strSMS = tmpSMS[0][1]
                  strTOAPPEND = chrCFGvalue.decode('cp1252') + strSMS + CONST_ENDTEXT
                  arrTask.append(strTOAPPEND)
            arrTask.append(CONST_TASK_MFIN)

          elif strTask == CONST_TASK_CHR:                                                                                     # SET HARD RESET
            printOUT(" [%s] TAREA: Reinicio de fabrica" % str(cantSessions) )
            arrTask = []
            strTOAPPEND = CONST_TASK_INIT + CONST_TASK_C + CONST_TASK_CHR_BYTE.decode('cp1252')
            arrTask.append(strTOAPPEND)

          elif strTask == CONST_TASK_CCE:                                                                                     # SET CLEAN ERRORS
            printOUT(" [%s] TAREA: Reinicio de indicadores de error" %str(cantSessions) )
            arrTask = []
            strTOAPPEND = CONST_TASK_INIT + CONST_TASK_C + CONST_TASK_CCE_BYTE.decode('cp1252')
            arrTask.append(strTOAPPEND)

          elif strTask == CONST_TASK_CSR:                                                                                     # SET SOFT RESET
            printOUT(" [%s] TAREA: Reinicio de cartel" % str(cantSessions) )
            arrTask = []
            strTOAPPEND = CONST_TASK_INIT + CONST_TASK_C + CONST_TASK_CSR_BYTE.decode('cp1252')
            arrTask.append(strTOAPPEND)

          elif strTask == CONST_TASK_CSH:                                                                                     # SET HOURTIME
            printOUT(" [%s] TAREA: Configuracion de hora" % str(cantSessions) )
            arrTask = []
            dataTime = chr(datetime.now().hour).decode('cp1252') + chr(datetime.now().minute).decode('cp1252') + chr(datetime.now().second).decode('cp1252')
            strTOAPPEND = CONST_TASK_INIT + CONST_TASK_C + CONST_TASK_CSH_BYTE.decode('cp1252')
            arrTask.append(strTOAPPEND)

          else:
            arrTask = []

        return arrTask

      else:
        return []

    except Exception as e:
      traceback.print_exc(file=sys.stdout)
      return []
