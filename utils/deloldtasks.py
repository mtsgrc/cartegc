#!/usr/bin/env python
# -*- coding: utf-8 -*-

import sqlite3
import os
from datetime import datetime

DAYS_OBS=7

BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))

pathDB = BASE_DIR + "/db.sqlite3"

def getDatetime():
  return datetime.strftime(datetime.now(),"%Y/%m/%d %H:%M:%S")

def deleteCompleteTasks():
  conn = sqlite3.connect(pathDB)
  cursor = conn.cursor()
  cursorTasks = conn.cursor()

  strQuery = "SELECT * FROM maindb_task WHERE status='c' and datejoin<=date('now','-%s day')" % (str(DAYS_OBS))
  cursorTasks.execute(strQuery)
  arrTasks = cursorTasks.fetchall()
  arrIdTasks = []
  for xTask in arrTasks:
    arrIdTasks.append(str(xTask[0]))
  strQuery = "DELETE FROM maindb_task WHERE status='c' and datejoin<=date('now','-%s day')" % (str(DAYS_OBS))
  cursor.execute(strQuery)
  conn.commit()

  print ("[%s] Tareas completas obsoletas: %s %s" % ( getDatetime(), str(conn.total_changes) , [ "" , "(%s)" % (','.join(arrIdTasks)) ][ len(arrIdTasks)>0 ]) )

deleteCompleteTasks()                 # Borramos tareas completas
