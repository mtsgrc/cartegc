#!/usr/bin/env bash

DIR_ACTUAL="$( cd `dirname "${BASH_SOURCE[0]}"` && pwd )"
DIR_ROOT="$(dirname "$DIR_ACTUAL")"

_USER="root"

_CMDSYNCTIME="/bin/bash $DIR_ACTUAL/synctime.sh"
_CMDSYNCTIME_DESCR="# Sincronizacion de hora con servicor NTP cada 6 horas"
_CMDSYNCTIME_LOGG="$DIR_ACTUAL/log/syncstime.log"

_CMDDELOLDTASKS="python $DIR_ACTUAL/deloldtasks.py"
_CMDDELOLDTASKS_DESCR="# Borrado de tareas completas con mas de 1 semana"
_CMDDELOLDTASKS_LOGG="$DIR_ACTUAL/log/deloldtasks.log"

_CMDDATEDTASKS="python $DIR_ACTUAL/checkdatedtasks.py"
_CMDDATEDTASKS_DESCR="# Actualizacion de tareas programadas"
_CMDDATEDTASKS_LOGG="$DIR_ACTUAL/log/datedtasks.log"


# INSTALLERS
function installDEP() {

  sudo apt-get install ntp                                                          # Sincronizador de hora
  sudo apt-get install python2.6 python2.6-dev
  sudo apt-get install python-pip
  sudo pip install Django==1.6.2

}

# CONFIG TIME
function configureTIME() {

  sudo cp /etc/localtime /etc/localtime-old                                         # Backup
  ln -sf /usr/share/zoneinfo/America/Argentina/Buenos_Aires /etc/locatime           # Set localtime
  sudo service ntp stop; sudo ntpdate pool.ntp.org; sudo service ntp start          # Stop service, sync time, Start service
  /sbin/hwclock --systohc                                                           # Copy datetime to RTC (hardware clock)

}

function configureCRON() {

  CRONTAB_SYNCTIME="* */6 * * * $_CMDSYNCTIME >> $_CMDSYNCTIME_LOGG 2>&1 $_CMDSYNCTIME_DESCR\n"
  CRONTAB_DELOLDTASKS="* */6 * * * $_CMDDELOLDTASKS >> $_CMDDELOLDTASKS_LOGG 2>&1 $_CMDDELOLDTASKS_DESCR\n"
  CRONTAB_DATEDTASKS="*/1 * * * * $_CMDDATEDTASKS >> $_CMDDATEDTASKS_LOGG 2>&1 $_CMDDATEDTASKS_DESCR\n\n"

  CRONTAB_LIMITLOGS="* * */1 * * tail -n 100 $_CMDSYNCTIME_LOGG > $_CMDSYNCTIME_LOGG\n* * */1 * * tail -n 100 $_CMDDELOLDTASKS_LOGG > $_CMDDELOLDTASKS_LOGG\n* * */1 * * tail -n 100 $_CMDDATEDTASKS_LOGG > $_CMDDATEDTASKS_LOGG"

  echo -e "$CRONTAB_SYNCTIME" > mycron_tmp
  echo -e "$CRONTAB_DELOLDTASKS" >> mycron_tmp
  echo -e "$CRONTAB_DATEDTASKS" >> mycron_tmp
  echo -e "$CRONTAB_LIMITLOGS" >> mycron_tmp

  crontab mycron_tmp
  rm mycron_tmp

}

installDEP

configureTIME

configureCRON


echo -e "Finalizando...\n"
