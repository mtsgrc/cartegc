#!/usr/bin/env python
# -*- coding: utf-8 -*-

import sqlite3
import os
from datetime import datetime

BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))

pathDB = BASE_DIR + "/db.sqlite3"

def getDatetime():
  return datetime.strftime(datetime.now(),"%Y/%m/%d %H:%M:%S")

def getNextTasks():
  try:
    conn = sqlite3.connect(pathDB)
    cursorTasks = conn.cursor()
    strQuery = "SELECT * FROM maindb_task WHERE todate<=datetime('now') AND status='i';"
    cursorTasks.execute(strQuery)
    arrTasks = cursorTasks.fetchall()
    arrIdTasks = []
    for xTask in arrTasks:
      arrIdTasks.append(str(xTask[0]))
    print ("[%s] Tareas programadas para hacer: %s %s" % ( getDatetime(), str(len(arrTasks)) , [ "" , "(%s)" % (','.join(arrIdTasks)) ][ len(arrIdTasks)>0 ]) )
    return arrTasks

  except Exception as e:
    print ("Error ocurrido: %s" % ( str(e) ) )

def setNextTasks(arrTasks):
  try:
    if arrTasks:

      conn = sqlite3.connect(pathDB)
      cursorTasks = conn.cursor()

      for iTask in arrTasks:
        tmpID = iTask[0]
        tmpSign = iTask[1]
        tmpTypeTask = iTask[4]
        tmpContent = iTask[6]
        strQuerySign = ""
        print ( "TypeTask: %s" % (tmpTypeTask) )

        if tmpContent:

          if tmpTypeTask == 'b1':
            strQuerySign = "UPDATE maindb_cartel SET lastBroadcasting='%s' WHERE id=%s" % ( tmpContent, str(tmpSign))

          elif tmpTypeTask == 'm':
            strQuerySign = "UPDATE maindb_cartel SET lastMessages='%s' WHERE id=%s" % ( tmpContent, str(tmpSign) )

          else:
            pass

          if strQuerySign:
            cursorSign = conn.cursor()
            cursorSign.execute(strQuerySign)
            #connSign.commit

        cursorTask = conn.cursor()
        strQueryTask = "UPDATE maindb_task SET todate=null, content=null WHERE id=%s" % (str(tmpID))
        cursorTask.execute(strQueryTask)
        conn.commit()

      conn.close()

  except Exception as e:
    print ("Error ocurrido: %s" % ( str(e) ) )



arrTasksToChange = getNextTasks()     # Buscamos tareas programadas con fecha "actual"
setNextTasks(arrTasksToChange)        # Cambiamos horario y de programacion a nulo, contenido a nulo, y modificamos el cartel

